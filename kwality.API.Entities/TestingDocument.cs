﻿using System;
using System.Collections.Generic;

namespace kwality.API.Entities
{
    public class TestingDocument
    {
        public Guid Id { get; set; }
        public Guid TestingPlanId { get; set; }
        public Guid TeamId { get; set; }
        public string Name { set; get; }
        public List<TestCaseDocument> TestCaseDocuments { get; set; }
    }
}