﻿using System;
using System.Collections.Generic;

namespace kwality.API.Entities
{
    public class TestingPlan
    {
        public Guid Id { get; set; }
        public Guid TeamId { get; set; }
        public string Name { set; get; }
        public string Description { get; set; }
        public List<TestCase> TestCases { get; set; }
    }
}