﻿using System;
using System.Collections.Generic;

namespace kwality.API.Entities
{
    public class Team
    {
        public Guid Id { get; set; }
        public Guid OwnerId { get; set; }
        public List<Guid> Members { get; set; }
        public List<Guid> PendingInvites { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
    }

    public class TestCaseStep
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
    }
}