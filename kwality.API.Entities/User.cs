﻿using System;
using System.Collections.Generic;

namespace kwality.API.Entities
{
    public class User
    {
        public Guid Id { get; set; }
        public string GoogleId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public List<TeamInvitation> Invitations { get; set; }
    }

    public class TeamInvitation
    {
        public Guid TeamId { get; set; }
        public string TeamName { get; set; }
    }
}
