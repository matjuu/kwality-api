using System;

namespace kwality.API.Entities
{
    public class TestCaseDocument
    {
        public Guid Id { get; set; }
        public TestCase TestCase { get; set; }
        public TestCaseOutcome Outcome { get; set; }
        public string Comment { get; set; }
    }
}