﻿using System;
using System.Collections.Generic;

namespace kwality.API.Entities
{
    public class TestCase
    {
        public Guid Id { get; set; }
        public Guid TeamId { get; set; }
        public Guid TestingPlanId { get; set; }
        public string Name { set; get; }
        public string Description { get; set; }
        public string Prerequisites { get; set; }
        public List<TestCaseStep> Steps { get; set; }
        public string Expectation { get; set; }
    }
}