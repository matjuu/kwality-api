﻿namespace kwality.API.Entities
{
    public enum TestCaseOutcome
    {
        NotTested = 0,
        Green = 1,
        Yellow = 2,
        Red = 3
    }
}