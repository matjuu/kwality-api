using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;
using Moq;
using NUnit.Framework;
using kwality.API.Repository;
using kwality.API.Services;
using kwality.API.Services.Exceptions;

namespace kwality.API.Services.Tests
{
    [TestFixture]
    public class TestingPlanServiceTests
    {
        private MockRepository _mockRepository;

        private Mock<ITestingPlanRepository> _mockTestingPlanRepository;
        private Mock<ITeamRepository> _mockTeamRepository;

        [SetUp]
        public void SetUp()
        {
            _mockRepository = new MockRepository(MockBehavior.Strict);

            _mockTestingPlanRepository = _mockRepository.Create<ITestingPlanRepository>();
            _mockTeamRepository = _mockRepository.Create<ITeamRepository>();
        }

        [TearDown]
        public void TearDown()
        {
            this._mockRepository.VerifyAll();
        }

        [Test]
        public async Task Given_TestingPlanRequest_Assert_TestingPlan_Object_Is_Created_And_Returned()
        {
            // Arrange
            var testingPlanRequest = new TestingPlanRequest
            {
                Name = "Testing plan"
            };

            // Act
            TestingPlanService service = this.CreateService();
            var testingPlan = await service.Create(testingPlanRequest, Guid.NewGuid(), Guid.NewGuid());

            // Assert
            Assert.That(testingPlan, Is.Not.Null);
            Assert.That(testingPlan.Name, Is.EqualTo("Testing plan"));
        }

        [Test]
        public async Task Given_TestingPlanRequest_With_NoName_Assert_TestingPlan_BadRequestException_IsThrown()
        {
            // Arrange
            var testingPlanRequest = new TestingPlanRequest
            {
                Name = string.Empty
            };

            // Act
            TestingPlanService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<BadRequestException>(
                async () => await service.Create(testingPlanRequest, Guid.NewGuid(), Guid.NewGuid()));
        }

        [Test]
        public async Task Given_TestingPlanRequest_With_RandomUserId_Assert_TestingPlan_BadRequestException_IsThrown()
        {
            // Arrange
            _mockTeamRepository.Setup(x => x.GetById(It.IsAny<Guid>()))
                .ReturnsAsync(() => new Team {Members = new List<Guid> {Guid.NewGuid()}});
            var testingPlanRequest = new TestingPlanRequest
            {
                Name = string.Empty
            };

            // Act
            TestingPlanService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<BadRequestException>(
                async () => await service.Create(testingPlanRequest, Guid.NewGuid(), Guid.NewGuid()));
        }

        [Test]
        public async Task Given_TestingPlanRequest_Assert_TestingPlan_Object_Is_Updated_And_Returned()
        {
            // Arrange
            var testingPlanRequest = new TestingPlanRequest
            {
                Name = "Updated testing plan"
            };

            // Act
            TestingPlanService service = this.CreateService();
            var testingPlan = await service.Update(testingPlanRequest, Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid());

            // Assert
            Assert.That(testingPlan, Is.Not.Null);
            Assert.That(testingPlan.Name, Is.EqualTo("Updated testing plan"));
        }

        [Test]
        public async Task Given_Update_TestingPlanRequest_With_NoName_Assert_TestingPlan_BadRequestException_IsThrown()
        {
            // Arrange
            var testingPlanRequest = new TestingPlanRequest
            {
                Name = string.Empty
            };

            // Act
            TestingPlanService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<BadRequestException>(
                async () => await service.Update(testingPlanRequest, Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid()));
        }


        [Test]
        public async Task Given_Update_TestingPlanRequest_With_UserNotFromTeam_Assert_TestingPlan_Forbidden_IsThrown()
        {
            // Arrange
            var userId = Guid.NewGuid();
            _mockTeamRepository.Setup(x => x.GetById(It.IsAny<Guid>()))
                .ReturnsAsync(() => new Team { Members = new List<Guid> { Guid.NewGuid() } });
            var testingPlanRequest = new TestingPlanRequest
            {
                Name = string.Empty
            };

            // Act
            TestingPlanService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<ForbiddenException>(
                async () => await service.Update(testingPlanRequest, Guid.NewGuid(), Guid.NewGuid(), userId));
        }

        private TestingPlanService CreateService()
        {
            return new TestingPlanService(
                this._mockTestingPlanRepository.Object,
                this._mockTeamRepository.Object);
        }
    }
}
