using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;
using Moq;
using NUnit.Framework;
using kwality.API.Repository;
using kwality.API.Services.Exceptions;

namespace kwality.API.Services.Tests
{
    [TestFixture]
    public class TeamServiceTests
    {
        private MockRepository _mockRepository;
        
        private Mock<ITeamRepository> _mockTeamRepository;
        private Mock<IUserRepository> _mockUserRepository;

        [SetUp]
        public void SetUp()
        {
            this._mockRepository = new MockRepository(MockBehavior.Strict);

            this._mockTeamRepository = this._mockRepository.Create<ITeamRepository>();
            this._mockUserRepository = this._mockRepository.Create<IUserRepository>();
        }

        [TearDown]
        public void TearDown()
        {
            this._mockRepository.VerifyAll();
        }

        [Test]
        public async Task Given_TeamRequest_Assert_Team_Object_Is_Created_And_Returned()
        {
            // Arrange
            var teamRequest = new TeamRequest
            {
                Name = "TestName"
            };

            // Act
            TeamService service = this.CreateService();
            var team = await service.Create(teamRequest, Guid.NewGuid());


            // Assert
            Assert.That(team, Is.Not.Null);
            Assert.That(team.Name, Is.EqualTo("TestName"));
        }

        [Test]
        public async Task Given_TeamRequest_With_EmptyName_Assert_BadRequestExceptionIsThrown()
        {
            // Arrange
            var teamRequest = new TeamRequest
            {
                Name = string.Empty
            };

            // Act
            TeamService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<BadRequestException>(async () => await service.Create(teamRequest, Guid.NewGuid()));
        }

        [Test]
        public async Task Given_TeamRequest_Assert_Team_Object_Is_Updated_And_Returned()
        {
            // Arrange
            var teamRequest = new TeamRequest
            {
                Name = "UpdatedTestName",
                Description = "Description too!"
            };

            // Act
            TeamService service = this.CreateService();
            var team = await service.UpdateTeam(Guid.NewGuid(), teamRequest, Guid.NewGuid());


            // Assert
            Assert.That(team, Is.Not.Null);
            Assert.That(team.Name, Is.EqualTo("UpdatedTestName"));
            Assert.That(team.Description, Is.EqualTo("Description too!"));
        }

        [Test]
        public async Task Given_TeamRequest_With_NonExistingTeam_Assert_NotFoudnException_IsThrown()
        {
            // Arrange
            var teamRequest = new TeamRequest
            {
                Name = "UpdatedTestName",
                Description = "Description too!"
            };

            // Act
            TeamService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await service.UpdateTeam(Guid.NewGuid(), teamRequest, Guid.NewGuid()));
        }

        [Test]
        public async Task Given_TeamInvitationRequest_Assert_Invitation_Object_Is_Created_And_User_Is_Updated()
        {
            // Arrange
            var invitationRequest = new TeamInvitationRequest()
            {
                Email = "test@test.com"
            };

            // Act
            TeamService service = this.CreateService();
            await service.InviteToTeam(Guid.NewGuid(), invitationRequest, Guid.NewGuid());

            // Assert
            _mockUserRepository.Verify(x => x.GetById(It.IsAny<Guid>()), Times.Once);
            _mockUserRepository.Verify(x => x.Update(It.IsAny<User>()), Times.Once);
        }

        [Test]
        public async Task Given_TeamInvitationRequest_With_ExistingMemberId_Assert_BadRequestException_IsThrown()
        {
            // Arrange
            var memberGuid = Guid.NewGuid();
            _mockTeamRepository.Setup(x => x.GetById(It.IsAny<Guid>())).ReturnsAsync(() => new Team{Members = new List<Guid>{ memberGuid }});
            _mockUserRepository.Setup(x => x.GetByEmail(It.IsAny<string>())).ReturnsAsync(() => new User{Id = memberGuid});
            var invitationRequest = new TeamInvitationRequest()
            {
                Email = "test@test.com"
            };

            // Act
            TeamService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<BadRequestException>(async () => await service.InviteToTeam(Guid.NewGuid(), invitationRequest, Guid.NewGuid()));
        }

        [Test]
        public async Task Given_TeamInvitationRequest_With_NotExistingUser_Assert_NotFoundException_IsThrown()
        {
            // Arrange
            var invitationRequest = new TeamInvitationRequest()
            {
                Email = "notxisting@gmail.com"
            };
            _mockUserRepository.Setup(x => x.GetByEmail(invitationRequest.Email)).ReturnsAsync(() => null);

            // Act
            TeamService service = this.CreateService();

            // Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await service.InviteToTeam(Guid.NewGuid(), invitationRequest, Guid.NewGuid()));
        }



        private TeamService CreateService()
        {
            return new TeamService(
                this._mockTeamRepository.Object,
                this._mockUserRepository.Object);
        }
    }
}
