﻿using System;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using kwality.API.Entities;

namespace kwality.API.Host.MapperProfiles
{
    public class RequestToValueTypeProfile : Profile
    {
        public RequestToValueTypeProfile()
        {
            CreateMap<Contracts.ValueTypes.TestCaseStep, TestCaseStep>()
                .ForMember(x => x.Id, expression => expression.UseValue(Guid.NewGuid()));
        }
    }
}