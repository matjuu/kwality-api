﻿using AutoMapper;
using kwality.API.Contracts.Requests;
using kwality.API.Contracts.Responses;
using kwality.API.Entities;

namespace kwality.API.Host.MapperProfiles
{
    public class EntityToReponseProfile : Profile
    {
        public EntityToReponseProfile()
        {
            CreateMap<Team, TeamResponse>();
            CreateMap<TestingPlan, TestingPlanResponse>();
            CreateMap<TestCase, TestCaseResponse>();
            CreateMap<TestCaseStep, Contracts.ValueTypes.TestCaseStep>();
            CreateMap<TestingDocument, TestingDocumentResponse>();
            CreateMap<TestCaseDocument, TestCaseDocumentResponse>();
            CreateMap<TeamInvitation, Contracts.ValueTypes.TeamInvitation>();
        }
    }
}
