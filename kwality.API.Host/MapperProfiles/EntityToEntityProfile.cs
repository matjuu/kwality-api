using System;
using AutoMapper;
using kwality.API.Entities;

namespace kwality.API.Host.MapperProfiles
{
    public class EntityToEntityProfile : Profile
    {
        public EntityToEntityProfile()
        {
            CreateMap<TestCase, TestCaseDocument>()
                .ForMember(x => x.Id, expression => expression.UseValue(Guid.NewGuid()))
                .ForMember(x => x.TestCase, expression => expression.ResolveUsing(testCase => testCase));
        }
    }
}