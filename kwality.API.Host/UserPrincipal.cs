using System.Security.Principal;
using kwality.API.Entities;

namespace kwality.API.Host
{
    public class UserPrincipal : GenericPrincipal
    {
        public UserPrincipal(User user, IIdentity userIdentity) : base(userIdentity, new string[]{}) => User = user;

        public User User { get; }
    }
}