﻿using System;
using System.Net;
using System.Threading;
using kwality.API.Contracts.Responses;
using kwality.API.Entities;
using kwality.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace kwality.API.Host.Controllers
{
    [Authorize]
    [Route("api/users")]
    public class UsersController : Controller
    {
        private IUserService _userService;
        private User _user;

        public UsersController(IUserService userService)
        {
            _userService = userService;
            _user = ((UserPrincipal)Thread.CurrentPrincipal).User;
        }

        [HttpGet("me")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(UserResponse))]
        public IActionResult Get(Guid id)
        {
            return new OkObjectResult(_user);
        }
    }
}
