﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using kwality.API.Contracts.Requests;
using kwality.API.Contracts.Responses;
using kwality.API.Entities;
using kwality.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace kwality.API.Host.Controllers
{
    [Authorize]
    [Route("api/teams/{teamId:guid}/testingplans/{planId:guid}/testcases")]
    public class TestCasesController : Controller
    {
        private ITestCaseService _testCaseService;
        private User _user;

        public TestCasesController(ITestCaseService testCaseService)
        {
            _testCaseService = testCaseService;
            _user = ((UserPrincipal)Thread.CurrentPrincipal).User;
        }

        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<TestCaseResponse>))]
        public async Task<IActionResult> GetTestCases(Guid teamId, Guid planId)
        {
            var testCases = await _testCaseService.GetByTestingPlanId(teamId, planId, _user.Id);

            var response = Mapper.Map<IEnumerable<TestCaseResponse>>(testCases);

            return new OkObjectResult(response);
        }

        [HttpGet("{id:guid}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(TestCaseResponse))]
        public async Task<IActionResult> GetTestCaseById(Guid id, Guid teamId, Guid planId)
        {
            var testCase = await _testCaseService.GetById(id, teamId, planId, _user.Id);

            var response = Mapper.Map<TestCaseResponse>(testCase);

            return  new OkObjectResult(response);
        }

        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.Created, typeof(TestCaseResponse))]
        public async Task<IActionResult> CreateTestCase([FromBody]TestCaseRequest request, Guid teamId, Guid planId)
        {
            var testCase = await _testCaseService.Create(request, teamId, planId, _user.Id);

            return new CreatedResult($"/api/teams/{testCase.TeamId}/testingplans/{testCase.TestingPlanId}/testcases/{testCase.Id}", testCase);
        }

        [HttpPut("{id:guid}")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateTestCase([FromBody]TestCaseRequest request, Guid id, Guid teamId, Guid planId)
        {
            var testCase = await _testCaseService.Update(request,id, teamId, planId, _user.Id);

            var result = Mapper.Map<TestCaseResponse>(testCase);

            return new OkObjectResult(result);
        }

        [HttpDelete("{id:guid}")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteTestCase(Guid id, Guid teamId, Guid planId)
        {
            await _testCaseService.Delete(id, teamId, planId, _user.Id);

            return new OkResult();
        }
    }
}
