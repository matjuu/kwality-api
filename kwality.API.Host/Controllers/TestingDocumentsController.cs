﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using kwality.API.Contracts.Requests;
using kwality.API.Contracts.Responses;
using kwality.API.Entities;
using kwality.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace kwality.API.Host.Controllers
{
    [Authorize]
    [Route("api/teams/{teamId:guid}/testingdocuments")]
    public class TestingDocumentsController : Controller
    {
        private readonly ITestingDocumentsService _testingDocumentsService;
        private readonly User _user;

        public TestingDocumentsController(ITestingDocumentsService testingDocumentsService)
        {
            _testingDocumentsService = testingDocumentsService;
            _user = ((UserPrincipal)Thread.CurrentPrincipal).User;
        }

        [HttpGet]
        public async Task<IActionResult> GetTestingDocuments(Guid teamId)
        {
            var testingPlans = await _testingDocumentsService.GetByTeamId(teamId, _user.Id);

            var response = Mapper.Map<IEnumerable<TestingDocumentResponse>>(testingPlans);

            return new OkObjectResult(response);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetTestingDocument(Guid teamId, Guid id)
        {
            var testingPlan = await _testingDocumentsService.GetById(id, teamId, _user.Id);

            var response = Mapper.Map<TestingDocumentResponse>(testingPlan);

            return new OkObjectResult(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTestingPlan(Guid teamId, [FromBody] TestingDocumentRequest request)
        {
            var testingPlan = await _testingDocumentsService.Create(request, teamId, _user.Id);

            var response = Mapper.Map<TestingDocumentResponse>(testingPlan);

            return new CreatedResult($"api/teams/{response.TeamId}/testingplans/{response.Id}", response);
        }

        [HttpPut("{id:guid}/testcase/{testCaseId:guid}")]
        public async Task<IActionResult> UpdateTestCaseDocument(Guid teamId, Guid id, Guid testCaseId, [FromBody] TestingCaseDocumentRequest request)
        {
            var testingPlan = await _testingDocumentsService.UpdateTestCase(request, id, testCaseId, teamId, _user.Id);

            var response = Mapper.Map<TestingDocumentResponse>(testingPlan);

            return new OkObjectResult(response);
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTestingDocument(Guid teamId, Guid id)
        {
            await _testingDocumentsService.Delete(id, teamId, _user.Id);

            return new OkResult();
        }
    }
}
