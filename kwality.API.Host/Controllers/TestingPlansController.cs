﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using kwality.API.Contracts.Requests;
using kwality.API.Contracts.Responses;
using kwality.API.Entities;
using kwality.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace kwality.API.Host.Controllers
{
    [Authorize]
    [Route("api/teams/{teamId:guid}/testingplans")]
    public class TestingPlansController : Controller
    {
        private readonly ITestingPlanService _testingPlanService;
        private readonly User _user;

        public TestingPlansController(ITestingPlanService testingPlanService)
        {
            _testingPlanService = testingPlanService;
            _user = ((UserPrincipal)Thread.CurrentPrincipal).User;
        }

        [HttpGet]
        public async Task<IActionResult> GetTestingPlans(Guid teamId)
        {
            var testingPlans = await _testingPlanService.GetByTeamId(teamId, _user.Id);

            var response = Mapper.Map<IEnumerable<TestingPlanResponse>>(testingPlans);

            return new OkObjectResult(response);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetTestingPlan(Guid teamId, Guid id)
        {
            var testingPlan = await _testingPlanService.GetById(id, _user.Id);

            var response = Mapper.Map<TestingPlanResponse>(testingPlan);

            return new OkObjectResult(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTestingPlan(Guid teamId, [FromBody] TestingPlanRequest request)
        {
            var testingPlan = await _testingPlanService.Create(request, teamId, _user.Id);

            var response = Mapper.Map<TestingPlanResponse>(testingPlan);

            return new CreatedResult($"api/teams/{response.TeamId}/testingplans/{response.Id}", response);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTestingPlan(Guid teamId, Guid id, [FromBody] TestingPlanRequest request)
        {
            var testingPlan = await _testingPlanService.Update(request, id, teamId, _user.Id);

            var response = Mapper.Map<TestingPlanResponse>(testingPlan);

            return new OkObjectResult(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTestingPlan(Guid teamId, Guid id)
        {
            await _testingPlanService.Delete(id, _user.Id);

            return new OkResult();
        }
    }
}
