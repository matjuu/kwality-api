﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using kwality.API.Contracts.Requests;
using kwality.API.Contracts.Responses;
using kwality.API.Entities;
using kwality.API.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace kwality.API.Host.Controllers
{
    [Authorize]
    [Route("api/teams")]
    public class TeamsController : Controller
    {
        private readonly ITeamService _teamService;
        private readonly User _user;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
            _user = ((UserPrincipal)Thread.CurrentPrincipal).User;
        }

        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(IEnumerable<TeamResponse>))]
        public async Task<IActionResult> GetTeams()
        {
            var teams = await _teamService.GetByUserId(_user.Id);

            var response = Mapper.Map<IEnumerable<TeamResponse>>(teams);
            
            return new OkObjectResult(response);
        }

        [HttpGet("{id:guid}")]
        [SwaggerResponse((int)HttpStatusCode.OK, typeof(TeamResponse))]
        public async Task<IActionResult> GetTeam(Guid id)
        {
            var team = await _teamService.GetById(id, _user.Id);

            var response = Mapper.Map<TeamResponse>(team);

            return new OkObjectResult(response);
        }

        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.Created, typeof(TeamResponse))]
        public async Task<IActionResult> CreateTeam([FromBody] TeamRequest request)
        {
            var team = await _teamService.Create(request, _user.Id);

            var response = Mapper.Map<TeamResponse>(team);
            
            return new CreatedResult($"/api/teams/{team.Id}", response);
        }

        [HttpPut("{id:guid}")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        public async Task<IActionResult> UpdateTeam(Guid id, [FromBody]TeamRequest request)
        {
            var team = await _teamService.UpdateTeam(id, request, _user.Id);

            var response = Mapper.Map<TeamResponse>(team);

            return new OkObjectResult(response);
        }

        [HttpDelete("{id:guid}")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        public async Task<IActionResult> DeleteTeam(Guid id)
        {
            await _teamService.Delete(id, _user.Id);

            return new OkResult();
        }

        [HttpPost("{id:guid}/invitation")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        public async Task<IActionResult> InviteToTeam(Guid id, [FromBody] TeamInvitationRequest request)
        {
            await _teamService.InviteToTeam(id, request, _user.Id);

            return new OkResult();
        }

        [HttpPost("{id:guid}/invitation/accept")]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        public async Task<IActionResult> AcceptInvitation(Guid id)
        {
            await _teamService.AcceptInvitation(id, _user.Id);

            return new OkResult();
        }
    }
}
