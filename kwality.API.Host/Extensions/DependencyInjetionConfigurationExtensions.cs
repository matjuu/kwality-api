using kwality.API.Repository;
using kwality.API.Services;
using Microsoft.Extensions.DependencyInjection;

namespace kwality.API.Host.Extensions
{
    public static class DependencyInjetionConfigurationExtensions
    {
        public static IServiceCollection AddDependencyInjection(this IServiceCollection services)
        {
            services.RegisterApplicaitionServices();
            services.RegisterApplicaitionRepositories();

            return services;
        }

        private static void RegisterApplicaitionServices(this IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<ITeamService, TeamService>();
            services.AddTransient<ITestingPlanService, TestingPlanService>();
            services.AddTransient<ITestCaseService, TestCaseService>();
            services.AddTransient<ITestingDocumentsService, TestingDocumentsService>();
        }

        private static void RegisterApplicaitionRepositories(this IServiceCollection services)
        {
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<ITeamRepository, TeamRepository>();
            services.AddSingleton<ITestingPlanRepository, TestingPlanRepository>();
            services.AddSingleton<ITestingDocumentRepository, TestingDocumentRepository>();
        }
    }
}