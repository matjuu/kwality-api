using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using kwality.API.Contracts.Responses;
using kwality.API.Services;
using kwality.API.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Newtonsoft.Json;

namespace kwality.API.Host.Extensions
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                context.Response.OnStarting(state =>
                {
                    var httpContext = (HttpContext)state;
                    httpContext.Response.Headers.Add("Content-Type", "application/json");
                    return Task.CompletedTask;
                }, context);

                var errorResponse = new ErrorResponse();

                switch (exception)
                {
                    case BadRequestException b:
                        errorResponse.Message = b.Message;
                        errorResponse.Reason = b.Reason;
                        context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
                        break;
                    case NotFoundException n:
                        errorResponse.Message = n.Message;
                        errorResponse.Reason = n.Reason;
                        context.Response.StatusCode = (int) HttpStatusCode.NotFound;
                        break;
                    case ForbiddenException f:
                        errorResponse.Message = f.Message;
                        errorResponse.Reason = f.Reason;
                        context.Response.StatusCode = (int) HttpStatusCode.Forbidden;
                        break;
                    default:
                        errorResponse.Message = "Internal server error";
                        errorResponse.Reason = "InternalServerError";
                        context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                        break;
                }
                

                await context.Response.Body.FlushAsync();
                using (var writer = new StreamWriter(context.Response.Body))
                {
                    await writer.WriteAsync(JsonConvert.SerializeObject(errorResponse));
                }
            }            
        }
    }
}