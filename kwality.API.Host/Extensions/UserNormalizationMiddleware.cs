using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using kwality.API.Services;
using Microsoft.AspNetCore.Http;

namespace kwality.API.Host.Extensions
{
    public class UserNormalizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IUserService _userService;

        public UserNormalizationMiddleware(RequestDelegate next, IUserService userService)
        {
            _next = next;
            _userService = userService;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.User.Identity.IsAuthenticated)
            {
                var userClaims = context.User.Claims;
                var userFullName = context.User.Identity.Name;
                var externalUserId = userClaims.Single(x => x.Type.Contains("nameidentifier")).Value;
                var email = userClaims.Single(x => x.Type.Contains("email")).Value;

                var user = await _userService.GetByExternalId(externalUserId);
                if (user == null)
                {
                    var createdUser = await _userService.Create(externalUserId, userFullName, email);
                    user = createdUser;
                }

                Thread.CurrentPrincipal = new UserPrincipal(user, context.User.Identity);
            }

            await _next(context);
        }
    }
}