namespace kwality.API.Services.Exceptions
{
    public class BadRequestException : ApplicationException
    {
        public BadRequestException(string message, string reason) : base(message, reason)
        {
        }
    }
}