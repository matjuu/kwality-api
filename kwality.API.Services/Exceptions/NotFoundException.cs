namespace kwality.API.Services.Exceptions
{
    public class NotFoundException : ApplicationException
    {
        public NotFoundException(string message, string reason) : base(message, reason)
        {
        }
    }
}