namespace kwality.API.Services.Exceptions
{
    public class ForbiddenException : ApplicationException
    {
        public ForbiddenException(string message, string reason) : base(message, reason)
        {
        }
    }
}