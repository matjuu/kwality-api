using System;

namespace kwality.API.Services.Exceptions
{
    public abstract class ApplicationException : Exception
    {
        protected ApplicationException(string message, string reason) : base(message)
        {
            Reason = reason;
        }

        public string Reason { get; set; }
    }
}