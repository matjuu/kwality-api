using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Contracts.Responses;
using kwality.API.Entities;

namespace kwality.API.Services
{
    public interface ITestingDocumentsService
    {
        Task<IEnumerable<TestingDocument>> GetByTeamId(Guid teamId, Guid userId);
        Task<TestingDocument> GetById(Guid id, Guid teamId, Guid userId);
        Task<TestingDocument> Create(TestingDocumentRequest request, Guid teamId, Guid userId);
        Task<TestingDocument> UpdateTestCase(TestingCaseDocumentRequest request, Guid testingDocumentId, Guid testCaseId, Guid teamId, Guid userId);
        Task Delete(Guid id, Guid teamId, Guid userId);
    }
}