using System;
using System.Threading.Tasks;
using kwality.API.Entities;

namespace kwality.API.Services
{
    public interface IUserService
    {
        Task<User> GetById(Guid id);
        Task<User> GetByExternalId(string extrenalId);
        Task<User> GetByEmail(string email);
        Task<User> Create(string externalId, string fullName, string email);
    }
}