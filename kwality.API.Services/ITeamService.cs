using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;

namespace kwality.API.Services
{
    public interface ITeamService
    {
        Task<Team> GetById(Guid id, Guid userId);
        Task<IEnumerable<Team>> GetByUserId(Guid id);
        Task<Team> Create(TeamRequest request, Guid ownerId);
        Task<Team> UpdateTeam(Guid id, TeamRequest request, Guid userId);
        Task Delete(Guid id, Guid userId);
        Task InviteToTeam(Guid teamId, TeamInvitationRequest request, Guid userId);
        Task AcceptInvitation(Guid id, Guid userId);
    }
}