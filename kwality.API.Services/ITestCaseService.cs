using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;

namespace kwality.API.Services
{
    public interface ITestCaseService
    {
        Task<IEnumerable<TestCase>> GetByTestingPlanId(Guid teamId, Guid planId, Guid userId);
        Task<TestCase> GetById(Guid id, Guid teamId, Guid planId, Guid userId);
        Task<TestCase> Create(TestCaseRequest request, Guid teamId, Guid planId, Guid userId);
        Task<TestCase> Update(TestCaseRequest request, Guid id, Guid teamId, Guid planId, Guid userId);
        Task Delete(Guid id, Guid teamId, Guid planId, Guid userId);
    }
}