using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;
using kwality.API.Repository;
using kwality.API.Services.Exceptions;

namespace kwality.API.Services
{
    public class TestingDocumentsService : ITestingDocumentsService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ITestingPlanRepository _testingPlanRepository;
        private readonly ITestingDocumentRepository _testingDocumentRepository;

        public TestingDocumentsService(ITeamRepository teamRepository, ITestingPlanRepository testingPlanRepository, ITestingDocumentRepository testingDocumentRepository)
        {
            _teamRepository = teamRepository;
            _testingPlanRepository = testingPlanRepository;
            _testingDocumentRepository = testingDocumentRepository;
        }

        public async Task<IEnumerable<TestingDocument>> GetByTeamId(Guid teamId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            var testingDocuments = await _testingDocumentRepository.GetByTeamId(teamId);

            return testingDocuments;
        }

        public async Task<TestingDocument> GetById(Guid id, Guid teamId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            var testingDocument = await _testingDocumentRepository.GetById(id);
            if (testingDocument == null)
            {
                throw new NotFoundException("No such testing document was found", "TestingDocumentNotFound");
            }

            return testingDocument;
        }

        public async Task<TestingDocument> Create(TestingDocumentRequest request, Guid teamId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            var testingPlan = await _testingPlanRepository.GetById(request.TestingPlanId);
            if (testingPlan == null)
            {
                throw new NotFoundException("No such testing plan was found", "TestingPlanNotFound");
            }

            var testingDocuments = await _testingDocumentRepository.GetByTeamId(teamId);
            if (Enumerable.Any<TestingDocument>(testingDocuments, x => x.Name == request.Name))
            {
                throw new BadRequestException("Testing document name must be unique", "NameMustBeUnique");
            }

            var testingDocument = new TestingDocument
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                TeamId = teamId,
                TestingPlanId = testingPlan.Id,
                TestCaseDocuments = Mapper.Map<List<TestCaseDocument>>(testingPlan.TestCases)
            };

            await _testingDocumentRepository.Create(testingDocument);

            return testingDocument;
        }

        public async Task<TestingDocument> UpdateTestCase(TestingCaseDocumentRequest request, Guid testingDocumentId, Guid testCaseId, Guid teamId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            var testingDocument = await _testingDocumentRepository.GetById(testingDocumentId);
            if (testingDocument == null)
            {
                throw new NotFoundException("No such testing document was found", "TestingDocumentNotFound");
            }

            var testCase = Enumerable.SingleOrDefault<TestCaseDocument>(testingDocument.TestCaseDocuments, x => x.Id == testCaseId);
            if (testCase == null)
            {
                throw new NotFoundException("No such test case was found", "TestCaseNotFound");
            }

            var testCaseToUpdate = testingDocument.TestCaseDocuments.Find(x => x.Id == testCaseId);
            testCaseToUpdate.Outcome = (TestCaseOutcome)request.Outcome;
            testCaseToUpdate.Comment = request.Comment;

            await _testingDocumentRepository.Update(testingDocument);

            return testingDocument;
        }

        public async Task Delete(Guid id, Guid teamId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            var testingDocument = await _testingDocumentRepository.GetById(id);
            if (testingDocument == null)
            {
                throw new NotFoundException("No such testing document was found", "TestingDocumentNotFound");
            }

            await _testingDocumentRepository.Delete(testingDocument);
        }
    }
}