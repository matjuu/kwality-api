using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;
using kwality.API.Repository;
using kwality.API.Services.Exceptions;

namespace kwality.API.Services
{
    public class TestingPlanService : ITestingPlanService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ITestingPlanRepository _testingPlanRepository;

        public TestingPlanService(ITestingPlanRepository testingPlanRepository, ITeamRepository teamRepository)
        {
            _testingPlanRepository = testingPlanRepository;
            _teamRepository = teamRepository;
        }

        public async Task<TestingPlan> GetById(Guid id, Guid userId)
        {
            var teams = await _teamRepository.GetByUserId(userId);
            var testingPlan = await _testingPlanRepository.GetById(id);

            if (testingPlan == null)
            {
                throw new NotFoundException("Testing plan not found", "TestingPlanNotFound");
            }

            if (teams.All(t => t.Id != testingPlan.TeamId))
            {
                throw new ForbiddenException("User does not have permissions to access this testing plan", "InsufficientPermissions");
            }

            return testingPlan;
        }

        public async Task<IEnumerable<TestingPlan>> GetByTeamId(Guid teamId, Guid userId)
        {
            var teams = await _teamRepository.GetByUserId(userId);
            var team = teams.SingleOrDefault(t => t.Id == teamId);

            if (team == null)
            {
                throw new ForbiddenException("User does not have permissions to access this team", "InsufficientPermissions");
            }

            var testingPlans = await _testingPlanRepository.GetByTeamId(teamId);

            return testingPlans;
        }

        public async Task<TestingPlan> Create(TestingPlanRequest request, Guid teamId, Guid userId)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                throw new BadRequestException("Name must be provided", "NameMustBeProvided");
            }

            var teams = await _teamRepository.GetByUserId(userId);
            var team = Enumerable.SingleOrDefault(teams, t => t.Id == teamId);

            if (team == null)
            {
                throw new ForbiddenException("User does not have permissions to access this team", "InsufficientPermissions");
            }

            var testingPlans = await _testingPlanRepository.GetByTeamId(team.Id);

            foreach (var x in testingPlans)
            {
                if (x.Name == request.Name)
                {
                    throw new BadRequestException("Testing plan name must be unique per team", "NameMustBeUnique");
                }
            }

            var entity = new TestingPlan
            {
                Id = Guid.NewGuid(),
                TeamId = teamId,
                Name = request.Name,
                Description = request.Description,
                TestCases = new List<TestCase>()
            };

            var testingPlan = await _testingPlanRepository.Create(entity);

            return testingPlan;
        }

        public async Task<TestingPlan> Update(TestingPlanRequest request, Guid id, Guid teamId, Guid userId)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                throw new BadRequestException("Name must be provided", "NameMustBeProvided");
            }

            var teams = await _teamRepository.GetByUserId(userId);
            var testingPlan = await _testingPlanRepository.GetById(id);

            var team = teams.SingleOrDefault(x => x.Id == testingPlan.TeamId);

            if (team == null)
            {
                throw new ForbiddenException("User does not have permissions to access this testing plan", "InsufficientPermissions");
            }

            var testingPlans = await _testingPlanRepository.GetByTeamId(team.Id);

            foreach (var x in testingPlans)
            {
                if (x.Name == request.Name)
                {
                    throw new BadRequestException("Testing plan name must be unique per team", "NameMustBeUnique");
                }
            }

            testingPlan.Name = request.Name;
            testingPlan.Description = request.Description;

            await _testingPlanRepository.Update(testingPlan);

            return testingPlan;
        }

        public async Task Delete(Guid id, Guid userId)
        {
            var teams = await _teamRepository.GetByUserId(userId);
            var testingPlan = await _testingPlanRepository.GetById(id);

            if (Enumerable.All(teams, t => t.Id != testingPlan.TeamId))
            {
                throw new ForbiddenException("User does not have permissions to access this testing plan", "InsufficientPermissions");
            }

            if (testingPlan == null)
            {
                throw new NotFoundException("Testing plan was not found", "TestingPlanNotFound");
            }

            await _testingPlanRepository.Delete(testingPlan);
        }
    }
}