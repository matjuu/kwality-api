using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;
using kwality.API.Repository;
using kwality.API.Services.Exceptions;

namespace kwality.API.Services
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;

        public TeamService(ITeamRepository teamRepository, IUserRepository userRepository)
        {
            _teamRepository = teamRepository;
            _userRepository = userRepository;
        }

        public async Task<Team> GetById(Guid id, Guid userId)
        {
            var team = await _teamRepository.GetById(id);

            if (!CanUserAccessTheTeam(userId, team))
            {
                team = null;
            }

            return team;
        }

        public async Task<IEnumerable<Team>> GetByUserId(Guid id)
        {
            return await _teamRepository.GetByUserId(id);
        }

        public async Task<Team> Create(TeamRequest request, Guid ownerId)
        {
            if (string.IsNullOrWhiteSpace(request.Name))
            {
                throw new BadRequestException("Team name must be provided", "TeamNameNotProvided");
            }

            var team = new Team
            {
                Id = Guid.NewGuid(),
                PendingInvites = new List<Guid>(),
                Members = new List<Guid>{ownerId},
                Name = request.Name,
                Description = request.Description,
                OwnerId = ownerId
            };

            return await _teamRepository.Create(team);
        }

        public async Task<Team> UpdateTeam(Guid id, TeamRequest request, Guid userId)
        {
            var team = await _teamRepository.GetById(id);
            if (team == null)
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            if (team.OwnerId != userId)
            {
                throw new ForbiddenException("User does not have sufficient permissions to modify the team", "InsufficientPermissions");
            }

            team.Name = request.Name;
            team.Description = request.Description;

            await _teamRepository.Update(team);

            return team;
        }

        public async Task Delete(Guid id, Guid userId)
        {
            var team = await _teamRepository.GetById(id);

            if (team?.OwnerId != userId)
            {
                throw new ForbiddenException("Insufficient permissions to delete this team", "InsufficientPermissions");
            }

            await _teamRepository.Delete(team);
        }

        public async Task InviteToTeam(Guid teamId, TeamInvitationRequest request, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            var user = await _userRepository.GetByEmail(request.Email);
            if (user == null)
            {
                throw new NotFoundException("No user with such email was found in the system", "UserNotFound");
            }

            if (user.Invitations.Any(x => x.TeamId == team.Id))
            {
                throw new BadRequestException("User has already been invited", "UserAlreadyInvited");
            }

            user.Invitations.Add(new TeamInvitation{TeamId = team.Id, TeamName = team.Name});
            await _userRepository.Update(user);
        }

        public async Task AcceptInvitation(Guid id, Guid userId)
        {
            var team = await _teamRepository.GetById(id);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("No such team was found", "TeamNotFound");
            }

            var user = await _userRepository.GetById(userId);
            if (user == null)
            {
                throw new NotFoundException("No user with such email was found in the system", "UserNotFound");
            }

            if (user.Invitations.All(x => x.TeamId != id))
            {
                throw new BadRequestException("User isn't invited to this team", "NotInvited");
            }

            user.Invitations = user.Invitations.Where(x => x.TeamId != id).ToList();
            team.Members.Add(user.Id);

            await _teamRepository.Update(team);
            await _userRepository.Update(user);
        }

        private bool CanUserAccessTheTeam(Guid userId, Team team)
        {
            return team.Members.Contains(userId) || team.OwnerId == userId;
        }
    }
}