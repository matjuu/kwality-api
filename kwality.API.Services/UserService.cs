﻿using System;
using System.Threading.Tasks;
using kwality.API.Entities;
using kwality.API.Repository;

namespace kwality.API.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> GetById(Guid id)
        {
            return await _userRepository.GetById(id);
        }

        public async Task<User> GetByExternalId(string extrenalId)
        {
            return await _userRepository.GetByGoogleId(extrenalId);
        }

        public async Task<User> GetByEmail(string email)
        {
            return await _userRepository.GetByEmail(email);
        }

        public async Task<User> Create(string externalId, string fullName, string email)
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                GoogleId = externalId,
                FullName = fullName,
                Email = email
            };

            return await _userRepository.Create(user);
        }
    }
}
