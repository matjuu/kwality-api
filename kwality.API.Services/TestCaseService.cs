using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;
using kwality.API.Repository;
using kwality.API.Services.Exceptions;

namespace kwality.API.Services
{
    public class TestCaseService : ITestCaseService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly ITestingPlanRepository _testingPlanRepository;

        public TestCaseService(ITeamRepository teamRepository, ITestingPlanRepository testingPlanRepository)
        {
            _teamRepository = teamRepository;
            _testingPlanRepository = testingPlanRepository;
        }

        public async Task<IEnumerable<TestCase>> GetByTestingPlanId(Guid teamId, Guid planId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);    
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("Team not found", "TeamNotFound");
            }

            var testingPlan = await _testingPlanRepository.GetById(planId);
            if (testingPlan == null)
            {
                throw new NotFoundException("Testing plan not found", "TestingPlanNotFound");
            }

            return testingPlan.TestCases;
            ;        }

        public async Task<TestCase> GetById(Guid id, Guid teamId, Guid planId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("Team not found", "TeamNotFound");
            }

            var testingPlan = await _testingPlanRepository.GetById(planId);
            if (testingPlan == null)
            {
                throw new NotFoundException("Testing plan not found", "TestingPlanNotFound");
            }

            var testCase =  testingPlan.TestCases.SingleOrDefault(x => x.Id == id);

            if (testCase == null)
            {
                throw new NotFoundException("Test case was not found", "TestCaseNotFound");
            }

            return testCase;
        }

        public async Task<TestCase> Create(TestCaseRequest request, Guid teamId, Guid planId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("Team not found", "TeamNotFound");
            }

            var testingPlan = await _testingPlanRepository.GetById(planId);
            if (testingPlan == null)
            {
                throw new NotFoundException("Testing plan not found", "TestingPlanNotFound");
            }

            if (string.IsNullOrWhiteSpace(request.Name))
            {
                throw new BadRequestException("Name must be provided and not be empty", "NameNotProvided");
            }

            var testCase = new TestCase
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                TeamId = teamId,
                Description = request.Description,
                Steps = Mapper.Map<List<TestCaseStep>>(request.Steps),
                TestingPlanId = planId,
                Expectation = request.Expectation,
                Prerequisites = request.Prerequisites
            };

            if (testingPlan.TestCases.Any(x => x.Name == testCase.Name))
            {
                throw new BadRequestException("Test case name must be unique", "TestCaseNameMustBeUnique");
            }

            testingPlan.TestCases.Add(testCase);

            await _testingPlanRepository.Update(testingPlan);

            return testCase;
        }

        public async Task<TestCase> Update(TestCaseRequest request, Guid id, Guid teamId, Guid planId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("Team not found", "TeamNotFound");
            }

            var testingPlan = await _testingPlanRepository.GetById(planId);
            if (testingPlan == null)
            {
                throw new NotFoundException("Testing plan not found", "TestingPlanNotFound");
            }

            if (string.IsNullOrWhiteSpace(request.Name))
            {
                throw new BadRequestException("Name must be provided and not be empty", "NameNotProvided");
            }

            var testCase = testingPlan.TestCases.SingleOrDefault(x => x.Id == id);
            if (testCase == null)
            {
                throw new NotFoundException("Test case not found", "TestCaseNotFound");
            }

            testCase.Name = request.Name;
            testCase.Description = request.Description;
            testCase.Expectation = request.Expectation;
            testCase.Prerequisites = request.Prerequisites;
            testCase.Steps = Mapper.Map<List<TestCaseStep>>(request.Steps);

            await _testingPlanRepository.Update(testingPlan);

            return testCase;
        }

        public async Task Delete(Guid id, Guid teamId, Guid planId, Guid userId)
        {
            var team = await _teamRepository.GetById(teamId);
            if (team == null || !team.Members.Contains(userId))
            {
                throw new NotFoundException("Team not found", "TeamNotFound");
            }

            var testingPlan = await _testingPlanRepository.GetById(planId);
            if (testingPlan == null)
            {
                throw new NotFoundException("Testing plan not found", "TestingPlanNotFound");
            }

            var testCase = testingPlan.TestCases.SingleOrDefault(x => x.Id == id);
            if (testCase == null)
            {
                throw new NotFoundException("Test case not found", "TestCaseNotFound");
            }

            testingPlan.TestCases.Remove(testCase);

            await _testingPlanRepository.Update(testingPlan);
        }
    }
}