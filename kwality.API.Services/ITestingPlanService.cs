using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Contracts.Requests;
using kwality.API.Entities;

namespace kwality.API.Services
{
    public interface ITestingPlanService
    {
        Task<TestingPlan> GetById(Guid id, Guid userId);
        Task<IEnumerable<TestingPlan>> GetByTeamId(Guid teamId, Guid userId);
        Task<TestingPlan> Create(TestingPlanRequest request, Guid teamId, Guid userId);
        Task<TestingPlan> Update(TestingPlanRequest request, Guid teamId, Guid id, Guid userId);
        Task Delete(Guid id, Guid userId);
    }
}