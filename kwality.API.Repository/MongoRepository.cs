using MongoDB.Driver;

namespace kwality.API.Repository
{
    public abstract class MongoRepository
    {
        protected static MongoClient Client;
        protected static IMongoDatabase Database;

        public MongoRepository()
        {
            Client = new MongoClient("mongodb://admin:admin@ds123695.mlab.com:23695/kwality");
            Database = Client.GetDatabase("kwality");
        }
    }
}