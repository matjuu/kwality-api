using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Entities;

namespace kwality.API.Repository
{
    public interface ITestingPlanRepository
    {
        Task<TestingPlan> GetById(Guid id);
        Task<IEnumerable<TestingPlan>> GetByTeamId(Guid teamId);
        Task<TestingPlan> Create(TestingPlan testingPlan);
        Task Update(TestingPlan testingPlan);
        Task Delete(TestingPlan testingPlan);
    }
}