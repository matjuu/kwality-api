using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Entities;
using MongoDB.Driver;

namespace kwality.API.Repository
{
    public class TestingPlanRepository : MongoRepository, ITestingPlanRepository
    {
        private readonly IMongoCollection<TestingPlan> _collection;

        public TestingPlanRepository()
        {
            _collection = MongoRepository.Database.GetCollection<TestingPlan>(nameof(TestingPlan));
        }

        public async Task<TestingPlan> GetById(Guid id)
        {
            var cursor = await IMongoCollectionExtensions.FindAsync<TestingPlan>(_collection, x => x.Id == id);

            return cursor.SingleOrDefault();
        }

        public async Task<IEnumerable<TestingPlan>> GetByTeamId(Guid teamId)
        {
            var filter = Builders<TestingPlan>.Filter.Where(plan => plan.TeamId == teamId);
            var cursor = await IMongoCollectionExtensions.FindAsync(_collection, filter);

            var result = new List<TestingPlan>();

            while (cursor.MoveNext())
            {
                result.AddRange(cursor.Current);
            }

            return result;
        }

        public async Task<TestingPlan> Create(TestingPlan testingPlan)
        {
            await _collection.InsertOneAsync(testingPlan);

            return testingPlan;
        }

        public async Task Update(TestingPlan testingPlan)
        {
            await IMongoCollectionExtensions.ReplaceOneAsync(_collection, x => x.Id == testingPlan.Id, testingPlan);
        }

        public async Task Delete(TestingPlan testingPlan)
        {
            await IMongoCollectionExtensions.DeleteOneAsync<TestingPlan>(_collection, x => x.Id == testingPlan.Id);
        }
    }
}