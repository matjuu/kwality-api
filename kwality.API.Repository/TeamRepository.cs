using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Entities;
using MongoDB.Driver;

namespace kwality.API.Repository
{
    public class TeamRepository : MongoRepository, ITeamRepository
    {
        private readonly IMongoCollection<Team> _collection;

        public TeamRepository()
        {
            _collection = Database.GetCollection<Team>(nameof(Team));
        }

        public async Task<Team> GetById(Guid id)
        {
            var result = await _collection.FindAsync(x => x.Id == id);

            return result.SingleOrDefault();
        }

        public async Task<IEnumerable<Team>> GetByUserId(Guid userId)
        {
            var filter = Builders<Team>.Filter.Where(team => team.Members.Contains(userId));

            var cursor = await _collection.FindAsync(filter);

            var result = new List<Team>();

            while (cursor.MoveNext())
            {
                result.AddRange(cursor.Current);
            }

            return result;
        }

        public async Task<Team> Create(Team team)
        {
            await _collection.InsertOneAsync(team);

            return team;
        }

        public async Task Update(Team team)
        {
            await _collection.ReplaceOneAsync(x => x.Id == team.Id, team);
        }

        public async Task Delete(Team team)
        {
            await _collection.DeleteOneAsync(x => x.Id == team.Id);
        }
    }
}