using System;
using System.Threading.Tasks;
using kwality.API.Entities;

namespace kwality.API.Repository
{
    public interface IUserRepository
    {
        Task<User> GetById(Guid id);
        Task<User> GetByGoogleId(string googleId);
        Task<User> Create(User user);
        Task<User> GetByEmail(string email);
        Task<User> Update(User user);
    }
}