﻿using System;
using System.Threading.Tasks;
using kwality.API.Entities;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Driver;

namespace kwality.API.Repository
{
    public class UserRepository : MongoRepository, IUserRepository
    {
        private readonly IMongoCollection<User> _collection;
        private readonly IMemoryCache _cache;

        public UserRepository(IMemoryCache cache)
        {
            _cache = cache;
            _collection = Database.GetCollection<User>(nameof(User));
        }

        public async Task<User> GetById(Guid id)
        {
            if (_cache.TryGetValue(id, out User cachedUser))
            {
                return cachedUser;
            }

            var user = (await _collection.FindAsync(u => u.Id == id).ConfigureAwait(false)).SingleOrDefault();
            if (user != null)
            {
                _cache.CreateEntry(id).Value = user;
            }

            return user;
        }

        public async Task<User> GetByGoogleId(string googleId)
        {
            if (_cache.TryGetValue(googleId, out User cachedUser))
            {
                return cachedUser;
            }

            var user = (await _collection.FindAsync(u => u.GoogleId == googleId).ConfigureAwait(false)).SingleOrDefault();
            if (user != null)
            {
                _cache.CreateEntry(googleId).Value = user;
            }

            return user;
        }

        public async Task<User> GetByEmail(string email)
        {
            if (_cache.TryGetValue(email, out User cachedUser))
            {
                return cachedUser;
            }

            var user = (await _collection.FindAsync(u => u.Email == email).ConfigureAwait(false)).SingleOrDefault();
            if (user != null)
            {
                _cache.CreateEntry(email).Value = user;
            }

            return user;
        }

        public async Task<User> Update(User user)
        {
            _cache.Remove(user.Email);
            _cache.Remove(user.Id);
            _cache.Remove(user.GoogleId);

            await _collection.ReplaceOneAsync(x => x.Id == user.Id, user);

            return user;
        }

        public async Task<User> Create(User user)
        {
            await _collection.InsertOneAsync(user);

            return user;
        }
    }
}
