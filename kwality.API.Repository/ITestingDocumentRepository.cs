using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Entities;
using MongoDB.Driver;

namespace kwality.API.Repository
{
    public interface ITestingDocumentRepository
    {
        Task<TestingDocument> GetById(Guid id);
        Task<IEnumerable<TestingDocument>> GetByTeamId(Guid teamId);
        Task<TestingDocument> Create(TestingDocument testingDocument);
        Task Update(TestingDocument testingDocument);
        Task Delete(TestingDocument testingDocument);
    }

    public class TestingDocumentRepository : MongoRepository, ITestingDocumentRepository
    {
        private readonly IMongoCollection<TestingDocument> _collection;

        public TestingDocumentRepository()
        {
            _collection = Database.GetCollection<TestingDocument>(nameof(TestingDocument));
        }

        public async Task<TestingDocument> GetById(Guid id)
        {
            var cursor = await _collection.FindAsync(x => x.Id == id);

            return cursor.SingleOrDefault();
        }

        public async Task<IEnumerable<TestingDocument>> GetByTeamId(Guid teamId)
        {
            var filter = Builders<TestingDocument>.Filter.Where(document => document.TeamId == teamId);
            var cursor = await IMongoCollectionExtensions.FindAsync(_collection, filter);

            var result = new List<TestingDocument>();

            while (cursor.MoveNext())
            {
                result.AddRange(cursor.Current);
            }

            return result;
        }

        public async Task<TestingDocument> Create(TestingDocument testingDocument)
        {
            await _collection.InsertOneAsync(testingDocument);

            return testingDocument;
        }

        public async Task Update(TestingDocument testingDocument)
        {
            await _collection.ReplaceOneAsync(x => x.Id == testingDocument.Id, testingDocument);
        }

        public async Task Delete(TestingDocument testingDocument)
        {
            await _collection.DeleteOneAsync(x => x.Id == testingDocument.Id);
        }
    }
}