using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using kwality.API.Entities;

namespace kwality.API.Repository
{
    public interface ITeamRepository
    {
        Task<Team> GetById(Guid id);
        Task<IEnumerable<Team>> GetByUserId(Guid userId);
        Task<Team> Create(Team team);
        Task Update(Team team);
        Task Delete(Team team);
    }
}