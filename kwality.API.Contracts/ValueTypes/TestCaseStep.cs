using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.ValueTypes
{
    /// <summary>
    /// An object decribing a single step to be taken when asserting a test case
    /// </summary>
    public class TestCaseStep
    {
        /// <summary>
        /// Identifier for the step
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Step description
        /// </summary>
        [Required]
        public string Value { get; set; }
    }
}