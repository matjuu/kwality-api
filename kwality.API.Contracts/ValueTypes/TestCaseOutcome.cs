namespace kwality.API.Contracts.ValueTypes
{
    /// <summary>
    /// The outcome of going through a test case
    /// </summary>
    public enum TestCaseOutcome
    {
        NotTested = 0,
        Green = 1,
        Yellow = 2,
        Red = 3
    }
}