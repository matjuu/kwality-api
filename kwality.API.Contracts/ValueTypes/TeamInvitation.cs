using System;
using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.ValueTypes
{
    /// <summary>
    /// An object describing an invitation to a team
    /// </summary>
    public class TeamInvitation
    {
        /// <summary>
        /// Name of the team which the invitation came from
        /// </summary>
        [Required]
        public string TeamName { get; set; }

        /// <summary>
        /// The id of the team
        /// </summary>
        public Guid TeamId { get; set; }
    }
}