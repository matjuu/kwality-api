using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.Responses
{
    /// <summary>
    /// An object describing a Testing plan
    /// </summary>
    public class TestingPlanResponse
    {
        /// <summary>
        /// Testing plan id
        /// </summary>
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// Id of the team the testing plan belongs to
        /// </summary>
        [Required]
        public Guid TeamId { get; set; }

        /// <summary>
        /// Testing plan name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Testing plan description
        /// </summary>
        public string Description { get; set; }


        /// <summary>
        /// A list of test cases that the testing plan contains
        /// </summary>
        public IEnumerable<TestCaseResponse> TestCases { get; set; }
    }
}