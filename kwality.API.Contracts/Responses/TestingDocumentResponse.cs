using System;
using System.Collections.Generic;

namespace kwality.API.Contracts.Responses
{
    /// <summary>
    /// An object describing a testing document 
    /// </summary>
    public class TestingDocumentResponse
    {
        /// <summary>
        /// Id of the testing document
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Id of the testing plan that the document is based on
        /// </summary>
        public Guid TestingPlanId { get; set; }
        /// <summary>
        /// Id of the team the document belongs to
        /// </summary>
        public Guid TeamId { get; set; }
        /// <summary>
        /// Name of the document
        /// </summary>
        public string Name { set; get; }
        /// <summary>
        /// A list of test cases that have been generated for the document
        /// </summary>
        public List<TestCaseDocumentResponse> TestCaseDocuments { get; set; }
    }
}