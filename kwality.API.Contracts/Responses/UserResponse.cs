using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using kwality.API.Contracts.ValueTypes;

namespace kwality.API.Contracts.Responses
{
    /// <summary>
    /// An object describing an invitation to a team
    /// </summary>
    public class UserResponse
    {
        /// <summary>
        /// User id
        /// </summary>
        [Required]
        public Guid Id { get; set; }

        /// <summary>
        /// Name of the user
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Email of the user
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// A list of invitations to different teams
        /// </summary>
        public IEnumerable<TeamInvitation> Invitations { get; set; }
    }
}