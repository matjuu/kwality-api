using System;
using kwality.API.Contracts.ValueTypes;

namespace kwality.API.Contracts.Responses
{
    /// <summary>
    /// A test case of a test document
    /// </summary>
    public class TestCaseDocumentResponse
    {
        /// <summary>
        /// Id of the test case
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// The test case of the document
        /// </summary>
        public TestCaseResponse TestCase { get; set; }
        /// <summary>
        /// Thhe outcome of the test case
        /// </summary>
        public TestCaseOutcome Outcome { get; set; }
        /// <summary>
        /// A comment describing the outcome of the test case
        /// </summary>
        public string Comment { get; set; }
    }
}