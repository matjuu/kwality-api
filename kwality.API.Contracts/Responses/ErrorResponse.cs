﻿namespace kwality.API.Contracts.Responses
{
    /// <summary>
    /// An object representing an error
    /// </summary>
    public class ErrorResponse
    {
        /// <summary>
        /// The reason string code
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// The message describing the error in more detail
        /// </summary>
        public string Message { get; set; }
    }
}