using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using kwality.API.Contracts.ValueTypes;

namespace kwality.API.Contracts.Responses
{
    /// <summary>
    /// An object describing a Test Case
    /// </summary>
    public class TestCaseResponse
    {
        /// <summary>
        /// Test case id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The id of the team the test case belongs to
        /// </summary>
        public Guid TeamId { get; set; }

        /// <summary>
        /// The id of the parent testing plan which the test case belongs to
        /// </summary>
        public Guid TestingPlanId { get; set; }

        /// <summary>
        /// Test case name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Test case description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Requirements before starting going through the test steps
        /// </summary>
        public string Prerequisites { get; set; }

        /// <summary>
        /// A list of steps needed for assertion of test case validity
        /// </summary>
        [Required]
        public List<TestCaseStep> Steps { get; set; }

        /// <summary>
        /// The expected state after the test steps have been completed
        /// </summary>
        [Required]
        public string Expectation { get; set; }
    }
}