﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.Responses
{
    /// <summary>
    /// An object describing a Team
    /// </summary>
    public class TeamResponse
    {
        /// <summary>
        /// Team Id
        /// </summary>
        [Required]
        public Guid Id { get; set; }
        /// <summary>
        /// Team name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Team description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// A list of users that have been
        /// </summary>
        public IEnumerable<Guid> PendingInvites { get; set; }

        /// <summary>
        /// The owner of the team
        /// </summary>
        [Required]
        public Guid OwnerId { get; set; }

        /// <summary>
        /// A list of users that are part of this team
        /// </summary>
        public IEnumerable<Guid> Members { get; set; }
    }
}
