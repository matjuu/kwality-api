﻿using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.Requests
{
    /// <summary>
    /// An object describing a Team
    /// </summary>
    public class TeamRequest
    {
        /// <summary>
        /// Team name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Team description
        /// </summary>
        public string Description { get; set; } 
    }
}
