using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.Requests
{
    /// <summary>
    /// An object describing an invitation to a team
    /// </summary>
    public class TeamInvitationRequest
    {
        /// <summary>
        /// Email of the user you're inviting
        /// </summary>
        [Required]
        public string Email { get; set; }
    }
}