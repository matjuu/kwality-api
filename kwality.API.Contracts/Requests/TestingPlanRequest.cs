using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.Requests
{
    /// <summary>
    /// An object describing a Testing plan
    /// </summary>
    public class TestingPlanRequest
    {
        /// <summary>
        /// Testing plan name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Testing plan description
        /// </summary>
        public string Description { get; set; }
    }
}