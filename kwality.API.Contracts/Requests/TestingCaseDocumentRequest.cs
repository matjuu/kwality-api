using System.ComponentModel.DataAnnotations;
using kwality.API.Contracts.ValueTypes;

namespace kwality.API.Contracts.Requests
{
    /// <summary>
    /// An object a tesing document update request
    /// </summary>
    public class TestingCaseDocumentRequest
    {
        /// <summary>
        /// Testing outcome
        /// </summary>
        [Required]
        public TestCaseOutcome Outcome { get; set; }

        /// <summary>
        /// A comment for further describing the outcome if needed
        /// </summary>
        public string Comment { get; set; }

    }
}