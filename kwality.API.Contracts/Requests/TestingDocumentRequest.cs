using System;
using System.ComponentModel.DataAnnotations;

namespace kwality.API.Contracts.Requests
{
    /// <summary>
    /// An object describing a Testing plan
    /// </summary>
    public class TestingDocumentRequest
    {
        /// <summary>
        /// Testing document name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The id of the testing plan the document should be based on
        /// </summary>
        [Required]
        public Guid TestingPlanId { get; set; }
    }
}